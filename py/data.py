#! /usr/bin/env python3
import json
import os
import re
from statistics import median
from operator import itemgetter
from pprint import pprint

# CONSTANTS
CONST_N = 'n'
CONST_M = 'm'
CONST_MPI = 'mpi'
CONST_MV_MPI = 'mv_mpi'
CONST_SEQ = 'seq'
CONST_MV_SEQ = 'mv_seq'
CONST_ID = 'id'
CONST_NODES = 'nodes'
CONST_NTASKS = 'n_tasks_per_node'
CONST_CPUTASKS = 'cpus_per_task'
CONST_REP = 'repetition'

CONST_HOME = os.environ['HOME']
CONST_PATH_RESULTS = '/Projects/MatrixMultiplication/results/'
CONST_PATH_ERRORS = '/Projects/MatrixMultiplication/error/'

CONST_PATH_RESULTS_MV_MPI = CONST_HOME + CONST_PATH_RESULTS + 'mv/' + CONST_MPI
CONST_PATH_RESULTS_MV_SEQ = CONST_HOME + CONST_PATH_RESULTS + 'mv/' + CONST_SEQ

CONST_PATH_ERROS_MV_MPI = CONST_HOME + CONST_PATH_ERRORS + 'mv/' + CONST_MPI
CONST_PATH_ERROS_MV_SEQ = CONST_HOME + CONST_PATH_ERRORS + 'mv/' + CONST_SEQ


class ParseResults:
    lmv_mpi = []
    lmv_seq = []
    errors = dict(mv_mpi=dict(), mv_seq=dict())

    def parse_mv_mpi(self):
        for filename in os.listdir(CONST_PATH_RESULTS_MV_MPI):
            with open(CONST_PATH_RESULTS_MV_MPI + '/' + filename) as f:

                info = re.split(r'[_.]', filename)
                try:
                    data = json.load(f)
                except ValueError:
                    data = dict(time=-1, mC=[])
                    self.errors[CONST_MV_MPI][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_NODES] = info[1]
                data[CONST_NTASKS] = info[2]
                data[CONST_CPUTASKS] = info[3]
                data[CONST_N] = info[4]
                data[CONST_M] = info[5]
                data[CONST_REP] = info[6]
                self.lmv_mpi.append(data)
                self.lmv_mpi = sorted(self.lmv_mpi, key=itemgetter(CONST_ID))

    def parse_mv_seq(self):
        for filename in os.listdir(CONST_PATH_RESULTS_MV_SEQ):
            with open(CONST_PATH_RESULTS_MV_SEQ + '/' + filename) as f:

                info = re.split(r'[_.]', filename)

                try:
                    data = json.load(f)
                except ValueError:
                    data = {"time": -1, "hist": []}
                    self.errors[CONST_MV_SEQ][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_N] = info[1]
                data[CONST_M] = info[2]
                data[CONST_REP] = info[3]
                self.lmv_seq.append(data)
                self.lmv_seq = sorted(self.lmv_seq, key=itemgetter(CONST_ID))

    def check_mv_mpi_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_MV_MPI):
            with open(CONST_PATH_ERROS_MV_MPI + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_MV_MPI][info[0]] = [CONST_PATH_ERROS_MV_MPI + '/' + filename, txt]

    def check_mv_seq_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_MV_SEQ):
            with open(CONST_PATH_ERROS_MV_SEQ + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_MV_SEQ][info[0]] = [CONST_PATH_ERROS_MV_SEQ + '/' + filename, txt]


class ParallelStats:
    lmedian_by_key = dict()

    def median_by_key(self, key, data):
        if key not in self.lmedian_by_key:
            self.lmedian_by_key.update({key: []})

        for i in range(0, len(data), 3):
            m = median([data[i][key], data[i + 1][key], data[i + 2][key]])
            self.lmedian_by_key[key].append(m)


ps = ParseResults()
ps.parse_mv_seq()
ps.check_mv_seq_errors()
ps.check_mv_mpi_errors()

pprint(ps.lmv_seq)

for seq in ps.lmv_seq:
    print(str(seq["id"])+" "+str(seq["n"])+" "+str(seq["m"])+" "+str(seq["time"]))
