#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

# os.environ["TMPDIR"] = '/tmp/scratch/damcoutinho2'

Time = namedtuple("Time", "d h m s")

t = Time(0, 0, 28, 0)

i_minimun = 1;
cpus_per_task = 1
n_tasks_per_node = 1
nodes = 1
min = 9  # 13
max = 13 # 19
rMax = 3  # 3

for i in range(min, max):
    n = 2 ** i
    for j in range(min, max):
        m = 2 ** j
        for repetion in range(1, (rMax + 1)):
            rc = subprocess.call(
                ['sbatch', '--job-name=MVSeq', '--partition=test',
                 '--output=../results/mv/seq/%j_' + str(n) + '_' + str(m) + '_' + str(repetion) + '.json',
                 '--error=../error/mv/seq/%j_' + str(n) + '_' + str(m) + '_' + str(repetion) + '.err',
                 '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
                 '--cpus-per-task=' + str(cpus_per_task), '--exclusive', '--mem=120G',
                 '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s),
                 '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
                 '../sh/mv_seq.sh', str(n), str(m)])

# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
# subprocess.check_output -- check the command
