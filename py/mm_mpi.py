#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

Time = namedtuple("Time", "d h m s")

i_minimun = 1;
cpus_per_task = 1
min = 9  
max = 13 
rMax = 3  
ma=2048
nb=2048

for i in range(min, max):
    na = 2 ** i
    mb = 2 ** i
    for nodes in [1,2]:
        if nodes != 1:
            i_minimun = 5
        else:
            i_minimun = 1
        for i in range(i_minimun, 6):
            n_tasks_per_node = 2 ** i
            for repetion in range(1, (rMax + 1)):
                if (nodes * n_tasks_per_node) <= 32:
                    t = Time(0, 0, 5, 0)
                elif (nodes * n_tasks_per_node) <= 64:
                    t = Time(0, 0, 10, 0)
                elif (nodes * n_tasks_per_node) <= 128:
                    t = Time(0, 0, 10, 0)
                else:
                    t = Time(0, 0, 10, 0)
                
                rc = subprocess.call(
                    ['sbatch', '--job-name=MMMPI', '--partition=test',
                     '--output=../results/mm/mpi/%j_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                         cpus_per_task) + '_' + str(na) + '_' + str(ma) + '_' + str(nb) + '_' + str(mb)  + '_' + str(repetion) + '.json',
                     '--error=../error/mm/mpi/%j_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                         cpus_per_task) + '_' + str(na) + '_' + str(ma) + '_' + str(nb) + '_' + str(mb)  + '_' + str(repetion) + '.err',
                     '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
                     '--cpus-per-task=' + str(cpus_per_task), '--exclusive', '--mem=120G',
                     '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s),
                     '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
                     '../sh/mm_mpi.sh', str(na), str(ma),str(nb), str(mb)])

# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
# subprocess.check_output -- check the command
