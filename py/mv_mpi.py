#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

Time = namedtuple("Time", "d h m s")
t = Time(0, 0, 29, 0)

i_minimun = 1;
cpus_per_task = 1
min = 9  # 13
max = 13 # 18
rMax = 3  # 3

for i in range(min, max):
    n = 2 ** i
    for j in range(min, max):
        m = 2 ** j
        for nodes in [1,2,4]:
            if nodes != 1:
                i_minimun = 5
            else:
                i_minimun = 1
            for i in range(i_minimun, 6):
                n_tasks_per_node = 2 ** i
                for repetion in range(1, (rMax + 1)):                   
                    rc = subprocess.call(
                        ['sbatch', '--job-name=MVMPI',
                         '--output=../results/mv/mpi/%j_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                             cpus_per_task) + '_' + str(n) + '_' + str(m) + '_' + str(repetion) + '.json',
                         '--error=../error/mv/mpi/%j_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                             cpus_per_task) + '_' + str(n) + '_' + str(m) + '_' + str(repetion) + '.err',
                         '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
                         '--cpus-per-task=' + str(cpus_per_task), '--exclusive', '--mem=120G',
                         '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s),
                         '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
                         '../sh/mv_mpi.sh', str(n), str(m)])

# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
# subprocess.check_output -- check the command
