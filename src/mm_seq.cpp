#include <iostream>

#define ROW_SZ 6
#define COL_SZ 3
using namespace std;

void fill(int * const mat, int r, int c){

	for (int ctR = 0; ctR < r; ctR++){
		for (int ctC = 0; ctC < c; ctC++){
			mat[ctR*c + ctC] = 1;
		}
	}
}

int main( int argc, char** argv ) {

	int rSzA = atoi(argv[1]);
	int cSzA = atoi(argv[2]);
	int rSzB = atoi(argv[3]);
	int cSzB = atoi(argv[4]);
	int rSzC = rSzA;
	int cSzC = cSzB;

	double *mA = new (std::nothrow) double * [ rSzA*cSzA ];
	double *mB = new (std::nothrow) double * [ rSzB*cSzB ];
	double *mC = new (std::nothrow) double * [ rSzC*cSzC ];

	fillMat(mA, rSzA, cSzA);
	fillMat(mB, CrSzB, cSzB);
	fillMat(mC, rSzC, cSzC);


	for(int ctR = 0; ctR < rSzA; ctR++){
		for(int ctC = 0; ctC < cSzA; ctC++){
			mC[ctR] += mA[ctR*cSzA + ctC]*mB[ctC];
		}
	}
	

	delete [] mA;
	delete [] mB;
	delete [] mC;

	return EXIT_SUCCESS;
}


void multiplyMatrix(int **mA, int **mB, int **mC,
	int aRows, int aCols, int bCols){

	int tmp;

	for (int ctR = 0; ctR < aRows; ctR++)
		for (int ctC = 0; ctC < bCols; ctC++) {
			tmp = 0.0;
			
			for (int k = 0; k < aCols; k++)
				tmp += mA[ctR][k] * mB[k][ctC];
			
			mC[ctR][ctC] = tmp;
		}
	}