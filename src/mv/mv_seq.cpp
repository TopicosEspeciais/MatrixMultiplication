#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include "../../../rapidjson/document.h"
#include "../../../rapidjson/writer.h"
#include "../../../rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;


double ** allocateMat( int rSz, int cSz ) {

	double **ptM = NULL;
	int ctR, ctC;


  /* Allocate pointers to all rows. */
	ptM = new (std::nothrow) double * [ rSz ];
	if ( ptM == 0 )
		return NULL;

  /* Allocate each row individually */
	for ( ctR = 0; ctR < rSz; ctR++ ) {
		*(ptM + ctR ) = new (std::nothrow) double [ cSz ];
		if ( *(ptM + ctR )  == 0 )
			return NULL;
	}

	return ptM;
}

double **deletemat( double ** mat,	int rSz ) {
	int ctR;

  /* Delete each row previously allocated. */
	for ( ctR = 0; ctR < rSz; ctR++ )
		if ( *(mat + ctR ) != NULL )
			delete [] *(mat + ctR) ;

  /* Safely deletes the array of pointers now. */
		if  ( mat != NULL )
			delete [] mat;

  return NULL;  /* NULL */
}

void  fill(double *mat,int rSz, int cSz ){

	time_t t;
    srand((unsigned)time(&t)); 

	for (int ctR = 0; ctR < rSz; ctR++){
		for (int ctC = 0; ctC < cSz; ctC++){
			mat[ctR*cSz + ctC] = ((double) (rand()) / RAND_MAX)*10;
		}
	}
}

void  fill(double **mat,int rSz, int cSz ){

	time_t t;
    srand((unsigned)time(&t)); 

	for (int ctR = 0; ctR < rSz; ctR++){
		for (int ctC = 0; ctC < cSz; ctC++){
			mat[ctR][ctC] = ((double) (rand()) / RAND_MAX)*10;
		}
	}
}

void display ( double * const mat, int rSz, int cSz){

	for (int ctR = 0; ctR < rSz; ctR++){
		cout << "|";
		for (int ctC = 0; ctC < cSz; ctC++)
			cout << mat[ ctR*cSz + ctC ] << " ";
		cout << "|\n";
	}
}

struct timespec My_diff(struct timespec start, struct timespec end) {
    struct timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

int main( int argc, char** argv ) {

	struct timespec timeInit, timeEnd;

	int rSzA = atoi(argv[1]);
	int cSzA = atoi(argv[2]);

	double **mA = allocateMat(rSzA,cSzA);
	double *mB = new (std::nothrow) double [ cSzA ];
	double *mC = new (std::nothrow) double [ rSzA ];

	fill(mA,rSzA,cSzA);
	fill(mB,cSzA,1);

    if (clock_gettime(CLOCK_REALTIME, &timeInit)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

	for(int ctR = 0; ctR < rSzA; ctR++){
		for(int ctC = 0; ctC < cSzA; ctC++){
			mC[ctR] += mA[ctR][ctC]*mB[ctC];
		}
	}

    if (clock_gettime(CLOCK_REALTIME, &timeEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    //constante equivalente a segundos em nanosegundos
    double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    //calculando a  diferença entre os dois tempos
    struct timespec timeDiff;
    timeDiff = My_diff(timeInit, timeEnd);
    //somando o tempo em segundo mais o tempo restante em nanosegundos.
    double processTime = (timeDiff.tv_sec + (double) timeDiff.tv_nsec / ONE_SECOND_IN_NANOSECONDS);
    //-----------JSON-----------
    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();
    writer.Key("time");
    //writer.SetMaxDecimalPlaces(10);
    writer.Double(processTime);
    writer.Key("mC");
    writer.StartArray();
    for (unsigned i = 0; i < rSzA; i++)
        writer.Double(mC[i]);
    writer.EndArray();
    writer.EndObject();

    cout << s.GetString() << endl;

	deletemat(mA,rSzA);
	delete [] mB;
	delete [] mC;

	return EXIT_SUCCESS;
}