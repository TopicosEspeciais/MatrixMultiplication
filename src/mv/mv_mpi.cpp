#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include "mpi.h"
#include "../../../rapidjson/document.h"
#include "../../../rapidjson/writer.h"
#include "../../../rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

double ** allocateMat( int rSz, int cSz ) {

	double **ptM = NULL;
	int ctR, ctC;


  /* Allocate pointers to all rows. */
	ptM = new (std::nothrow) double * [ rSz ];
	if ( ptM == 0 )
		return NULL;

  /* Allocate each row individually */
	for ( ctR = 0; ctR < rSz; ctR++ ) {
		*(ptM + ctR ) = new (std::nothrow) double [ cSz ];
		if ( *(ptM + ctR )  == 0 )
			return NULL;
	}

	return ptM;
}

double **deletemat( double ** mat,	int rSz ) {
	int ctR;

  /* Delete each row previously allocated. */
	for ( ctR = 0; ctR < rSz; ctR++ )
		if ( *(mat + ctR ) != NULL )
			delete [] *(mat + ctR) ;

  /* Safely deletes the array of pointers now. */
		if  ( mat != NULL )
			delete [] mat;

  return NULL;  /* NULL */
}

void  fill(double *mat,int rSz, int cSz,int rank ){

	time_t t;
    srand((unsigned)time(&t)*rank); 

	for (int ctR = 0; ctR < rSz; ctR++){
		for (int ctC = 0; ctC < cSz; ctC++){
			mat[ctR*cSz + ctC] = ((double) (rand()) / RAND_MAX)*10;
		}
	}
}

void  fill(double **mat,int rSz, int cSz,int rank ){

	time_t t;
    srand((unsigned)time(&t)*rank); 

	for (int ctR = 0; ctR < rSz; ctR++){
		for (int ctC = 0; ctC < cSz; ctC++){
			mat[ctR][ctC] = ((double) (rand()) / RAND_MAX)*10;
		}
	}
}

void display ( double * const mat, int rSz, int cSz){

	for (int ctR = 0; ctR < rSz; ctR++){
		cout << "|";
		for (int ctC = 0; ctC < cSz; ctC++)
			cout << mat[ ctR*cSz + ctC ] << " ";
		cout << "|\n";
	}
}

int main( int argc, char** argv ) {

    char hostname[MPI_MAX_PROCESSOR_NAME];

    double pi, initial_time=0, final_time=0;
    int numTasks, rank, rc,len;

	int rSzA = atoi(argv[1]);
	int cSzA = atoi(argv[2]);

	rc = MPI_Init(&argc, &argv);

  	if (rc != MPI_SUCCESS) {
        printf("Error starting MPI program.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Get_processor_name(hostname, &len);

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //size of the mAPerTasktrix per process
	int rSzAPerTask = rSzA / numTasks;
	int cSzAPerTask = cSzA / numTasks;

	double **mAPerTask = allocateMat(rSzAPerTask,cSzA);
	double *mBPerTask = new (std::nothrow) double [ cSzAPerTask ];
	double *mCPerTask = new (std::nothrow) double [ rSzAPerTask ];
	for (int i = 0; i < rSzAPerTask; ++i)mCPerTask[i]=0;


	double *mB = new (std::nothrow) double [ cSzA ];
	double *mC = NULL;

	if (rank ==0)
	{
		mC = new (std::nothrow) double [ rSzA ];
		for (int i = 0; i < rSzA; ++i) mC[i]=0;
	
	}	

	fill(mAPerTask,rSzAPerTask,cSzA,rank);
	fill(mBPerTask,cSzAPerTask,1,rank);

	initial_time = MPI_Wtime();

	MPI_Allgather( mBPerTask, cSzAPerTask, MPI_DOUBLE, 
				   mB, cSzAPerTask, MPI_DOUBLE, MPI_COMM_WORLD );

	for(int ctR = 0; ctR < rSzAPerTask; ctR++){
		for(int ctC = 0; ctC < cSzAPerTask; ctC++){
			mCPerTask[ctR] += mAPerTask[ctR][ctC]*mBPerTask[ctC];
		}
	}

	MPI_Gather( mCPerTask, rSzAPerTask, MPI_DOUBLE, 
				mC, rSzAPerTask, MPI_DOUBLE, 0, MPI_COMM_WORLD );



    //-----------JSON-----------
    if(rank == 0){

    	final_time = MPI_Wtime();

        StringBuffer s;
        Writer<StringBuffer> writer(s);
        writer.StartObject();
        writer.Key("time");
        //writer.SetMaxDecimalPlaces(10);
        writer.Double(final_time - initial_time);
        writer.Key("mC");
        writer.StartArray();
        for (unsigned i = 0; i < rSzA; i++)
            writer.Double(mC[i]);
        writer.EndArray();
        writer.EndObject();

        cout << s.GetString() << endl;
    }
	deletemat(mAPerTask,rSzAPerTask);
	delete [] mBPerTask;
	delete [] mCPerTask;
	delete [] mB;

	if( rank == 0 ){
		delete [] mC;
	}

	MPI_Finalize();
	
	return EXIT_SUCCESS;
}