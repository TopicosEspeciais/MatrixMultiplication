#include <iostream>
#include "mpi.h"

#define ROW_SZ_A 1024
#define COL_SZ_A 1024
#define ROW_SZ_B 1024
#define COL_SZ_B 1
using namespace std;

void display ( int ** const mat, int r, int c );
int ** deletemat( int ** mat, int rSz );
int ** allocateMat( int rSz, int cSz );
void fillMat(int ** const mat,int * const array ,int r, int c,int rank);
void multiplyMatrix(int **mA, int *mB, int *mC,
	int aRows, int aCols, int bCols,int rank);

int main( int argc, char *argv[] ) {

	int numTasks, rank, rc;

	rc = MPI_Init(&argc, &argv);

	if (rc != MPI_SUCCESS) {
		printf("Error starting MPI program.\n");
		MPI_Abort(MPI_COMM_WORLD, rc);
	}

	MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int rowPartSize = ROW_SZ_A/numTasks;

	int **mA = allocateMat( rowPartSize, COL_SZ_A );
	int *mB = new int[rowPartSize];
	int *mB_All =  new int[ROW_SZ_A];
	int *mC = new int[rowPartSize];
	int *mC_All = new int[ROW_SZ_A];

	fillMat(mA,NULL, rowPartSize, COL_SZ_A,rank);
	fillMat(NULL, mB,rowPartSize, COL_SZ_B,rank);

	MPI_Allgather(mB, rowPartSize, MPI_INT, mB_All, rowPartSize, MPI_INT,
              MPI_COMM_WORLD);

	multiplyMatrix(mA, mB_All, mC,	rowPartSize, COL_SZ_A, COL_SZ_B,rank);

	MPI_Gather(mC, rowPartSize, MPI_INT, mC_All, rowPartSize, MPI_INT, 0, MPI_COMM_WORLD);

	if (rank == 0)
	{
		for (int ctR = 0; ctR < ROW_SZ_A; ctR++)	cout <<  mC_All[ctR] << endl;
	}
	
	deletemat( mA, rowPartSize );

	delete [] mB;
	delete [] mC;
	delete [] mB_All;
	delete []  mC_All;

	MPI_Finalize();
	
	return EXIT_SUCCESS;
}

void display ( int ** const mat, int rSz, int cSz ) {
	int r, c;

	for ( r = 0; r < rSz; r++ ) {
		cout << "|";
		for ( c = 0; c < cSz; c++ )
			cout << mat[ r ][ c ] << " ";
		cout << "|\n";
	}
}

int ** allocateMat( int rSz, int cSz ) {

	int **ptM = NULL;
	int ctR, ctC;


  /* Allocate pointers to all rows. */
	ptM = new (std::nothrow) int * [ rSz ];
	if ( ptM == 0 )
		return NULL;

  /* Allocate each row individually */
	for ( ctR = 0; ctR < rSz; ctR++ ) {
		*(ptM + ctR ) = new (std::nothrow) int [ cSz ];
		if ( *(ptM + ctR )  == 0 )
			return NULL;

    /* Fill up this new row of the mat. */
		for ( ctC = 0; ctC < cSz; ctC++ )
			ptM[ ctR ][ ctC ] = ctR*cSz + ctC + 1;
	}

	return ptM;
}

int **deletemat( int ** mat,	int rSz ) {
	int ctR;

  /* Delete each row previously allocated. */
	for ( ctR = 0; ctR < rSz; ctR++ )
		if ( *(mat + ctR ) != NULL )
			delete [] *(mat + ctR) ;

  /* Safely deletes the array of pointers now. */
		if  ( mat != NULL )
			delete [] mat;

  return NULL;  /* NULL */
}

void fillMat(int ** const mat, int * const array ,int r, int c,int rank){


	for (int ctC = 0; ctC < c; ++ctC){
		for (int ctR = 0; ctR < r; ++ctR){
			if(mat == NULL)
				array[ctR] = 1;
			else
				mat[ctR][ctC] = 1;
		}
	}
}

void multiplyMatrix(int **mA, int *mB, int *mC,
	int aRows, int aCols, int bCols,int rank){

	int tmp;

	for (int ctR = 0; ctR < aRows; ctR++)
		for (int ctC = 0; ctC < bCols; ctC++) {
			tmp = 0.0;
			
			for (int k = 0; k < aCols; k++){
				tmp += mA[ctR][k] * mB[k];
				printf("rank %d - mA[%d][%d]:%d * mB[%d]:%d = %d\n",rank,ctR,k,mA[ctR][k],k,mB[k],tmp);
			}
			
			mC[ctR] = tmp;
		}
	}