#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include "../../../rapidjson/document.h"
#include "../../../rapidjson/writer.h"
#include "../../../rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;


void  fill(double *mat,int rSz, int cSz ){

	time_t t;
	srand((unsigned)time(&t)); 

	for (int ctR = 0; ctR < rSz; ctR++){
		for (int ctC = 0; ctC < cSz; ctC++){
			mat[ctR*cSz + ctC] = ((double) (rand()) / RAND_MAX)*10;
		}
	}
}

void display ( double * const mat, int rSz, int cSz){

	for (int ctR = 0; ctR < rSz; ctR++){
		cout << "|";
		for (int ctC = 0; ctC < cSz; ctC++)
			cout << mat[ ctR*cSz + ctC ] << " ";
		cout << "|\n";
	}
}

struct timespec My_diff(struct timespec start, struct timespec end) {
	struct timespec temp;
	if ((end.tv_nsec - start.tv_nsec) < 0) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return temp;
}

int main( int argc, char** argv ) {

	struct timespec timeInit, timeEnd;

	int rSzA = atoi(argv[1]);
	int cSzA = atoi(argv[2]);
	int rSzB = atoi(argv[3]);
	int cSzB = atoi(argv[4]);

	double *mA = new (std::nothrow) double [ rSzA*cSzA ];
	double *mB = new (std::nothrow) double [ rSzB*cSzB ];
	double *mC = new (std::nothrow) double [ rSzA*cSzB ];

	fill(mA,rSzA,cSzA);
	fill(mB,rSzB,cSzB);

	if (clock_gettime(CLOCK_REALTIME, &timeInit)) {
		perror("clock gettime");
		exit(EXIT_FAILURE);
	}

	for(int ctR = 0; ctR < rSzA; ctR++){
		for(int ctC = 0; ctC < cSzB; ctC++){
			for(int k = 0; k < cSzA; k++){				
				mC[ctR*cSzB + ctC] += mA[ctR*cSzA + k]*mB[k*cSzB + ctC];
			}
		}
	}
	
	if (clock_gettime(CLOCK_REALTIME, &timeEnd)) {
		perror("clock gettime");
		exit(EXIT_FAILURE);
	}

    //constante equivalente a segundos em nanosegundos
	double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    //calculando a  diferença entre os dois tempos
	struct timespec timeDiff;
	timeDiff = My_diff(timeInit, timeEnd);
    //somando o tempo em segundo mais o tempo restante em nanosegundos.
	double processTime = (timeDiff.tv_sec + (double) timeDiff.tv_nsec / ONE_SECOND_IN_NANOSECONDS);
    //-----------JSON-----------
	StringBuffer s;
	Writer<StringBuffer> writer(s);
	writer.StartObject();
	writer.Key("time");
    //writer.SetMaxDecimalPlaces(10);
	writer.Double(processTime);
	writer.Key("mC");
	writer.StartArray();
	for (unsigned i = 0; i < rSzA*cSzB; i++)
		writer.Double(mC[i]);
	writer.EndArray();
	writer.EndObject();

	cout << s.GetString() << endl;

	delete [] mA;
	delete [] mB;
	delete [] mC;

	return EXIT_SUCCESS;
}