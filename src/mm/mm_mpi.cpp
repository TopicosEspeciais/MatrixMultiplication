#include <iostream>
#include <cmath>
#include <ctime>
#include <vector>
#include "mpi.h"
#include "../../../rapidjson/document.h"
#include "../../../rapidjson/writer.h"
#include "../../../rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;


void  fill(double *mat,int rSz, int cSz ){

	time_t t;
	srand((unsigned)time(&t)); 

	for (int ctR = 0; ctR < rSz; ctR++){
		for (int ctC = 0; ctC < cSz; ctC++){
			mat[ctR*cSz + ctC] = ((double) (rand()) / RAND_MAX)*10;
		}
	}
}

void display ( double * const mat, int rSz, int cSz){

	for (int ctR = 0; ctR < rSz; ctR++){
		cout << "|";
		for (int ctC = 0; ctC < cSz; ctC++)
			cout << mat[ ctR*cSz + ctC ] << " ";
		cout << "|\n";
	}
}

struct timespec My_diff(struct timespec start, struct timespec end) {
	struct timespec temp;
	if ((end.tv_nsec - start.tv_nsec) < 0) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return temp;
}

int main( int argc, char** argv ) {

	char hostname[MPI_MAX_PROCESSOR_NAME];

	double pi, initial_time=0, final_time=0;
    int numTasks, rank, rc,len;

	struct timespec timeInit, timeEnd;

	int rSzA = atoi(argv[1]);
	int cSzA = atoi(argv[2]);
	int rSzB = atoi(argv[3]);
	int cSzB = atoi(argv[4]);

	rc = MPI_Init(&argc, &argv);

  	if (rc != MPI_SUCCESS) {
        printf("Error starting MPI program.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    MPI_Get_processor_name(hostname, &len);

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int rSzAPerTask = rSzA / numTasks;
	int rSzBPerTask = rSzB / numTasks;    

	double *mA = new (std::nothrow) double [ rSzAPerTask*cSzA ];
	double *mB = new (std::nothrow) double [ rSzBPerTask*cSzB ];
	double *mC = new (std::nothrow) double [ rSzAPerTask*cSzB ];
	double * globalmC = NULL;

	for(int i = 0; i < rSzAPerTask*cSzB; i++){
		mC[i] = 0;
	}

	//only allocate the memory of the global result in the rank 0
	if( rank == 0 ){
		globalmC = new double [rSzA*cSzB];
	}

	fill(mA,rSzAPerTask,cSzA);
	fill(mB,rSzBPerTask,cSzB);

	initial_time = MPI_Wtime();

	//Define a derived datatype which correspond to one column of the matrix2
	MPI_Datatype MPI_Column_mB;
	MPI_Type_vector( rSzBPerTask, 1, cSzB, MPI_DOUBLE, &MPI_Column_mB );
	MPI_Type_commit( &MPI_Column_mB );	

		
	//temporary vector that will hold each column of the (global) matrix2
	double *tmp_column;
	tmp_column = new double[rSzB];
	
	//iterate over the columns of the (local) matrix2
	for(int j = 0; j < cSzB; j++){
		
		//Gather each column of the (global) matrix2 in the temporary column, for all the processes
		MPI_Allgather( &mB[j], 1, MPI_Column_mB, 
					   tmp_column, rSzBPerTask, MPI_DOUBLE, MPI_COMM_WORLD );
		
		
		//calculates the final result of the partial matrix C
		for(int i = 0; i < rSzAPerTask; i++){
			for(int k = 0; k < cSzA; k++){
				mC[i*cSzB + j] += mA[i*cSzA + k]*tmp_column[k];				
			}			
		}
		
	}	

	//Gather all the local results in one global result
	MPI_Gather( mC, rSzAPerTask*cSzB , MPI_DOUBLE, 
				globalmC, rSzAPerTask*cSzB, MPI_DOUBLE, 0, MPI_COMM_WORLD );
	

    //-----------JSON-----------
    if(rank == 0){

    	final_time = MPI_Wtime();

        StringBuffer s;
        Writer<StringBuffer> writer(s);
        writer.StartObject();
        //writer.SetMaxDecimalPlaces(10);
        writer.Key("time");
        writer.Double(final_time - initial_time);
        writer.Key("globalmC");
        writer.StartArray();
        for (unsigned i = 0; i < rSzA*cSzB; i++)
            writer.Double(globalmC[i]);
        writer.EndArray();
        writer.EndObject();

        cout << s.GetString() << endl;

        delete [] globalmC;
    }

	delete [] mA;
	delete [] mB;
	delete [] mC;
	delete [] tmp_column;

	MPI_Finalize();

	return EXIT_SUCCESS;
}