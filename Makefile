############################# Makefile ##########################
CODE_MV = src/mv/
CODE_MM = src/mm/
BIN  = bin/
RESU = results/
ERR  = error/


all: mv_seq mv_mpi mm_seq mm_mpi

mv_seq:	$(CODE_MV)mv_seq.cpp
			g++ $(CODE_MV)mv_seq.cpp -lrt -lm  -o $(BIN)mv_seq 
mm_seq:	$(CODE_MM)mm_seq.cpp
			g++ $(CODE_MM)mm_seq.cpp -lrt -lm  -o $(BIN)mm_seq 			
mv_mpi:	$(CODE_MV)mv_mpi.cpp
			mpicxx $(CODE_MV)mv_mpi.cpp  -o $(BIN)mv_mpi
mm_mpi:	$(CODE_MM)mm_mpi.cpp
			mpicxx $(CODE_MM)mm_mpi.cpp  -o $(BIN)mm_mpi			

cbin:
			rm -rf  $(BIN)*
cresu: 
			rm -rf $(RESU)/mv/seq/* -rf $(RESU)/mm/seq/* $(RESU)/mv/mpi/* $(RESU)/mm/mpi/*
cerr: 
			rm -rf $(ERR)/mv/seq/* $(ERR)/mm/seq/* $(ERR)/mv/seq/* $(ERR)/mm/seq/*
cfiles: cresu cerr	
call: cbin cfiles

