/* 
 * Load modules:
 * module load compilers/gnu/6.4
 * 
 * Compile with:
 * g++ -o matrix_vector matrix_vector.cpp -Wall
 * 
 * Usage:
 * matrix_vector <read_file> <nº of rows of matrix 1> <<nº of columns of matrix 1> 
 * 			     <create output file> <debug> optionals: <filename_matrix> <filename_vector>
 * 
 * example: ./matrix_vector 0 100 200 0 2
 * example: ./matrix_vector 1 100 200 0 2 matrix.csv vector.csv
 */

#include <iostream>
using namespace std;

#include <iomanip>
#include <limits>

#include <string.h>

#include <sys/time.h>

#include <fstream>
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
        
#define DATE_FORMAT "%Y%m%d_%H%M%S"
#define DATE_SIZE 20
#define MINIMUM_VALUE -10000
#define MAXIMUM_VALUE 10000



// Main procedure
//----------------
int main(int argc, char** argv) {
	
	double time_ROI_begin, time_ROI_end;
	double time_begin, time_end;
	
	struct timeval t;
	
	gettimeofday(&t,NULL);
	time_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	bool read_file;
	int rows1, columns1;
	int rows_result;
	int output;
	int debug;
	string path1, path2;
	
	
	if( (argc != 6) and (argc != 8) ){
		cout << "Usage: " << endl 
			 << "mult_matrix <generate matrix or read file> " 
			 << "<number of rows of matrix1> <number of columns of matrix1> "
			 << "<create output file> <debug> " 
			 << "optionals: <path of matrix A> <path of vector x>"
			 << endl;
			 
		return -1;
	}
	
	read_file = atoi(argv[1]);
	
	rows1 = atoi(argv[2]);
	columns1 = atoi(argv[3]);

	
	output = atoi(argv[4]);
	debug = atoi(argv[5]);
	
	if( argc == 8 ){
		path1 = argv[6];
		path2 = argv[7];
	}
	
	rows_result = rows1;
	
	if( debug >= 3 ){
		cout << "INPUTS: " << endl
			 << "Read file: " << read_file << endl
			 << "Matrix 1: (" << rows1 << "x" << columns1 << ")" << endl
			 << "Output: " << output << endl
			 << "Debug: " << debug << endl << endl;
	}
	
	
  	
  	double **matrix;
	double *vector;
	double *result;
  	
  	matrix = new double *[rows1];
  	for(int i=0; i < rows1; i++){
		matrix[i] = new double[columns1];
	}
	
	vector = new double [columns1];
	
	result = new double [rows_result];
	
	
	if( debug >= 3 ){
		cout << "After dynamic allocation." << endl << endl;
	}
	
	
	if( !read_file ){
		
		time_t t;		
		srand((unsigned)time(&t));
		
		for(int i = 0; i<rows1; i++){
			for(int j = 0; j<columns1;j++){
				matrix[i][j] = MINIMUM_VALUE + 
								( (double)rand() / ( (double)( RAND_MAX /(MAXIMUM_VALUE-MINIMUM_VALUE) )) );
				
				//matrix[i][j] = MINIMUM_VALUE + (rand()%MAXIMUM_VALUE);
			}
		}
		
		for(int i = 0; i<columns1; i++){
			
			vector[i] = MINIMUM_VALUE + 
						( (double)rand() / ( (double)( RAND_MAX /(MAXIMUM_VALUE-MINIMUM_VALUE) )) );
			
			//vector[i] = MINIMUM_VALUE + (rand()%MAXIMUM_VALUE);
			
		}
		
		if( debug >= 2 ){
			cout << "GENERATED MATRIX'S" << endl << endl;
			
			cout << "Matrix A: " << endl;			
			for(int i = 0; i<rows1; i++){
				cout << "[";
				for(int j = 0; j<columns1;j++){
					cout << matrix[i][j] << " ";
				}
				cout << "]" << endl;
			}
			
			cout << endl << "Vector x: " << endl;
			cout << "[";		
			for(int i = 0; i<columns1; i++){
				cout << vector[i];
				if( i < columns1 -1 ){
					cout << endl;
				}
			}
			cout << "]" << endl;
		}
	}
	else{
		
		ifstream file1(path1);
		
		if( !file1.is_open() ){
			cout << "Unable to open file: " << path1 << endl;
			return -1;
		}
		
		ifstream file2(path2);
		
		if( !file2.is_open() ){
			cout << "Unable to open file: " << path2 << endl;
			return -1;
		}
		
		
		string read_value;
		char separator = ';';
		
		//TODO: check if the size of the file don't match with the size passed in the arguments and throw an error
		for(int i = 0; i < rows1; i++){
			for(int j = 0; j < columns1; j++){
				getline(file1, read_value, separator);
				matrix[i][j] = atof( read_value.c_str() );
			}
		}
		
		//TODO: check if the size of the file don't match with the size passed in the arguments and throw an error
		for(int i = 0; i < columns1; i++){
			getline(file2, read_value, separator);
			vector[i] = atof( read_value.c_str() );
		}
		
		
		if( debug >= 2 ){
			cout << "READED MATRIX'S" << endl << endl;
			
			cout << "Matrix A: " << endl;			
			for(int i = 0; i<rows1; i++){
				cout << "[";
				for(int j = 0; j<columns1;j++){
					cout << matrix[i][j] << " ";
				}
				cout << "]" << endl;
			}
			
			cout << endl << "Vector x: " << endl;
			cout << "[";		
			for(int i = 0; i<columns1; i++){
				cout << vector[i];
				if( i < columns1 -1 ){
					cout << endl;
				}
			}
			cout << "]" << endl;
		}
		
	}
	
	gettimeofday(&t,NULL);
	time_ROI_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	for(int i = 0; i < rows1; i++){
		for(int k = 0; k < columns1; k++){
			result[i] = result[i] + matrix[i][k]*vector[k];
		}
	}
	
	gettimeofday(&t,NULL);
	time_ROI_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	
	for (int i = 0; i < rows1; ++i)
		delete [] matrix[i];
	delete [] matrix;	
	delete [] vector;
	
  	
  	if( debug >= 1 ){
		cout << "ROI time: " << time_ROI_end - time_ROI_begin << " seconds." << endl;
	}
  	
  	
  	if( debug >= 2 ){
		
		cout << endl << "RESULT" << endl;
		cout << "[";	
		for(int i = 0; i<rows_result; i++){
			cout << setprecision(numeric_limits<double>::digits10) 
				 << result[i];
			if( i < rows_result-1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
  	
  	
  	gettimeofday(&t,NULL);
	time_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	if( debug >= 1 ){
		cout << "Total execution time: " << time_end - time_begin << " seconds." << endl;
	}
	
	
	if( output >= 1 ){
		
		char date[DATE_SIZE];
		time_t now = time(0);
		strftime(date, sizeof(date), DATE_FORMAT, localtime(&now));
		
		string csv_name = string("../outputs/") + string("output");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(1);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		char separator = ';';
		
		csv << "ROI time" << separator << time_ROI_end - time_ROI_begin << endl;
		csv << "Total time" << separator << time_end - time_begin << endl;
		csv << "nodes" << separator << 1 << endl;
		csv << "processes" << separator << 1 << endl;
		csv << "threads" << separator << 1 << endl;
		csv << "rows matrix 1" << separator << rows1 << endl;
		csv << "columns matrix 1" << separator << columns1 << endl;
		
		csv.close();
		
		
		if( output >= 2 ){
			
			csv_name = string("../outputs/") + string("result");
			csv_name += "_nodes_" + SSTR(1);
			csv_name += "_processes_" + SSTR(1);
			csv_name += "_threads_" + SSTR(1);
			csv_name += "_size_" + SSTR(rows1);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << csv_name << endl;
			}
			
			ofstream csv;
			csv.open(csv_name.c_str());
			
			for(int i=0; i<rows_result; i++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< result[i] << separator << endl;
			}
			
			csv.close();
		}

		
	}
	
	delete [] result;
	
	
	return 0;
}
