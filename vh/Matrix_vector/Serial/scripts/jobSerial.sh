#!/bin/bash 
#SBATCH --job-name=MxV_S
#SBATCH --output=../slurm_outputs/slurm-%j.out
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29


for i in {1..5};
do
	
	for size in 1024 2048 4096 8192 16384
	do	
	
		srun ../matrix_vector_Rev2 1 -10000 10000 $size 1024 1 0 
		
	done
	
done



