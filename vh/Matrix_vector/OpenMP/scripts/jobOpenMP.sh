#!/bin/bash 
#SBATCH --job-name=MxV_OMP
#SBATCH --output=../slurm_outputs/slurm-%j.out
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mem=120G
#SBATCH --hint=compute_bound
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29


for i in {1..5};
do
	for threads in 2 4 8 16 32
	do
		for rows in 1024 2048 4096 8192 16384
		do
			export OMP_NUM_THREADS=$threads
		
			srun ../matrix_vector_OpenMP_Rev2 1 -10000 10000 $rows 1024 1 0
			
		done
	
	done
done



