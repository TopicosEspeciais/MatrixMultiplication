/* 
 * Load modules:
 * module load compilers/gnu/6.4
 * 
 * Compile with:
 * g++ -o matrix_vector_OpenMP_Rev2 matrix_vector_OpenMP_Rev2.cpp -Wall -fopenmp
 * 
 * Usage:
 * matrix_vector_OpenMP_Rev1 <int or float> <minimum value> <maximum value> 
 * 				 			 <nº of rows of matrix 1> <nº of columns of matrix 1> 
 * 			     			 <create output file> <debug>
 * 
 * example: ./matrix_vector_OpenMP_Rev1 0 0 5 3 2 0 3
 * 
 * 			Multiplies one matrix of integers, varying from 0 to 5, of size 3x2 to one vector of integers of size 2x1, without creating an output file and debug option 3 (prints time results (1), debug informations (2) and generated and resulted matrixes (3))
 * 
 * example: ./matrix_vector_OpenMP_Rev1 1 -10000 10000 1024 1024 2 0 
 * 
 * 			Multiplies one matrix of floats, varying from -10000 to 10000 of size 1024x1024 to one vector of integers of size 1024x1, creating output files for generated and result matrixes and debug option 0 (don't print anyting)
 * 
 * Version control:
 * 
 * 	Rev2:
 * 		- Transformed the 2-d array to 1-d array to represent the matrix
 * 
 * 	Rev1:
 * 		- Removed the option of read from file;
 * 		- Save the matrixes used in the computation as output files;
 * 		- Read as arguments the possibilitie of generate int or float matrixes;
 * 		- Read as arguments the minimum and maximum values of the matrixes.
 * 
 */

#include <iostream>
using namespace std;

#include <omp.h> 

#include <iomanip>
#include <limits>
#include <string.h>
#include <sys/time.h>

#include <fstream>
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
        
#define DATE_FORMAT "%Y%m%d_%H%M%S"
#define DATE_SIZE 20



// Main procedure
//----------------
int main(int argc, char** argv) {
	
	double time_ROI_begin, time_ROI_end;
	double time_begin, time_end;
	
	struct timeval t;
	
	gettimeofday(&t,NULL);
	time_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	bool int_float;
	int minimum_value, maximum_value;
	int rows1, columns1;
	int output;
	int debug;
	
	int rows_result;
	
	
	if( argc != 8 ){
		cout << "Usage: " << endl 
			 << "mult_matrix <int or float> <minimum value> <maximum value> " 
			 << "<number of rows of matrix1> <number of columns of matrix1> "
			 << "<create output file> <debug> " 
			 << endl;
			 
		return -1;
	}
	
	//TODO: check consistency of those values
	int_float = atoi(argv[1]);
	minimum_value = atoi(argv[2]);	
	maximum_value = atoi(argv[3]);
	rows1 = atoi(argv[4]);
	columns1 = atoi(argv[5]);	
	output = atoi(argv[6]);
	debug = atoi(argv[7]);
	
	
	rows_result = rows1;
	
	if( debug >= 2 ){
		cout << "INPUTS: " << endl
			 << "Int or float: " << int_float << endl
			 << "Minimum value: " << minimum_value << endl
			 << "Maximum value: " << maximum_value << endl
			 << "Matrix 1: (" << rows1 << "x" << columns1 << ")" << endl
			 << "Output: " << output << endl
			 << "Debug: " << debug << endl << endl;
	}
	
	
  	
  	double *matrix;
	double *vector;
	double *result;
  	
  	matrix = new double [rows1*columns1];	
	vector = new double [columns1];	
	result = new double [rows_result];
	
	
	if( debug >= 2 ){
		cout << "After dynamic allocation." << endl << endl;
	}
	
	
	//GENERATING MATRIX A AND VECTOR X	
	srand((unsigned)time(0));
	
	for(int i = 0; i<rows1; i++){
		for(int j = 0; j<columns1;j++){
			
			if( int_float ){
				matrix[i*columns1 + j] = minimum_value + 
							   ( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
			}
			else{
				matrix[i*columns1 + j] = minimum_value + (rand()%maximum_value);
			}
		}
	}
	
	for(int i = 0; i<columns1; i++){
		
		if( int_float ){
			vector[i] = minimum_value + 
						( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
		}
		else{
			vector[i] = minimum_value + (rand()%maximum_value);
		}
		
	}
	
	if( debug >= 3 ){
		cout << "GENERATED MATRIX'S" << endl << endl;
		
		cout << "Matrix A: " << endl;			
		for(int i = 0; i<rows1; i++){
			cout << "[";
			for(int j = 0; j<columns1;j++){
				cout << matrix[i*columns1 + j] << " ";
			}
			cout << "]" << endl;
		}
		
		cout << endl << "Vector x: " << endl;
		cout << "[";		
		for(int i = 0; i<columns1; i++){
			cout << vector[i];
			if( i < columns1 -1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
	
	//Begin of the Region of Interest (ROI)
	
	gettimeofday(&t,NULL);
	time_ROI_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	int nthreads;
	#pragma omp parallel
	{		
		if( omp_get_thread_num() == 0 ){
			nthreads = omp_get_num_threads();			
			if( debug >= 2 ){			 
				cout << endl << "Number of threads is: " << nthreads << endl;
			}
		}
		
		#pragma omp for reduction(+:result[:rows1]) collapse(2)
		for(int i = 0; i < rows1; i++){
			for(int k = 0; k < columns1; k++){
				result[i] += matrix[i*columns1 + k]*vector[k];
			}
		}
	
	}
	
	gettimeofday(&t,NULL);
	time_ROI_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	//End of the Region of Interest (ROI)
	  	
  	if( debug >= 1 ){
		cout << "ROI time: " << time_ROI_end - time_ROI_begin << " seconds." << endl;
	}
  	
  	
  	if( debug >= 3 ){
		
		cout << endl << "RESULT" << endl;
		cout << "[";	
		for(int i = 0; i<rows_result; i++){
			cout << result[i];
			if( i < rows_result-1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
	
	char date[DATE_SIZE];
	time_t now = time(0);
	strftime(date, sizeof(date), DATE_FORMAT, localtime(&now));
	char separator = ';';
	
	if( output >= 2 ){
		
		//SAVING THE RESULT MATRIX
		string csv_name = string("../outputs/Rev2/") + string("result");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << endl << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		for(int i=0; i<rows_result; i++){
			csv << setprecision(numeric_limits<double>::digits10) 
				<< result[i] << separator << endl;
		}
		
		csv.close();
		
		
		//SAVING THE MATRIX A
		csv_name = string("../inputs/Rev2/") + string("matrix_A");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		csv.open(csv_name.c_str());
		
		for(int i=0; i<rows1; i++){
			for(int j=0; j<columns1; j++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< matrix[i*columns1 + j] << separator;
			}
			csv << endl;
		}
		
		csv.close();
		
		
		//SAVING THE VECTOR X
		csv_name = string("../inputs/Rev2/") + string("vector_x");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		csv.open(csv_name.c_str());
		
		for(int i=0; i<columns1; i++){
			csv << setprecision(numeric_limits<double>::digits10) 
				<< vector[i] << separator << endl;
		}
		
		csv.close();
		
	}
  	
  	
  	gettimeofday(&t,NULL);
	time_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	if( debug >= 1 ){
		cout << endl << "Total execution time: " << time_end - time_begin << " seconds." << endl << endl;
	}
	
	
	if( output >= 1 ){
		
		string csv_name = string("../outputs/Rev2/") + string("output");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		csv << "ROI time" << separator << time_ROI_end - time_ROI_begin << endl;
		csv << "Total time" << separator << time_end - time_begin << endl;
		csv << "nodes" << separator << 1 << endl;
		csv << "processes" << separator << 1 << endl;
		csv << "threads" << separator << nthreads << endl;
		csv << "rows matrix 1" << separator << rows1 << endl;
		csv << "columns matrix 1" << separator << columns1 << endl;
		
		csv.close();
		
	}
	
	//Free the memory
	delete [] matrix;		
	delete [] vector;	
	delete [] result;
	
	
	return 0;
}
