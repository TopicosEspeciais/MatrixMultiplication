#!/bin/bash 
#SBATCH --job-name=MxV_OMP
#SBATCH --output=../slurm_outputs/slurm-%j.out
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --mem=120G
#SBATCH --hint=compute_bound
#SBATCH --exclusive
#SBATCH --time=0-5:0

export TMPDIR=/tmp/scratch/$USER

for i in {1..5};
do
	for threads in 1 2 4 8 16 32
	do
		for rows in 1024 2048 4096 8192 16384
		do
			export OMP_NUM_THREADS=$threads
		
			srun ../matrix_vector_MPI_OpenMP_Rev1 1 -10000 10000 $rows 1024 1 0 4
			
		done
	
	done
done



