/* 
 * Program to calculate a matrix-vector multiplication (Ax = b)
 * 
 * Load modules:
 * module load compilers/gnu/6.4
 * module load libraries/openmpi/2.1.1-gnu-6.4
 * 
 * Compile with:
 * mpic++ -o matrix_vector_MPI_OpenMP_Rev1 matrix_vector_MPI_OpenMP_Rev1.cpp -Wall -fopenmp
 * 
 * Usage:
 * matrix_vector_MPI_OpenMP_Rev0 <int or float> <minimum value> <maximum value> 
 * 						  		 <nº of rows of matrix 1> <nº of columns of matrix 1> 
 * 			    		  		 <create output file> <debug> <nº of nodes>
 * 
 * example: ./matrix_vector_MPI_OpenMP_Rev0 0 0 5 3 2 0 3 2
 * 
 * 			Multiplies one matrix of integers, varying from 0 to 5, of size 3x2 to one vector of integers of size 2x1, without creating an output file, with debug option 3 (prints time results (1), debug informations (2) and generated and resulted matrixes (3)) and running in 2 different nodes
 * 
 * example: ./matrix_vector_MPI_OpenMP_Rev0 1 -10000 10000 1024 1024 2 0 2
 * 
 * 			Multiplies one matrix of floats, varying from -10000 to 10000 of size 1024x1024 to one vector of integers of size 1024x1, creating output files for generated and result matrixes, with debug option 0 (don't print anyting) and running in 2 different nodes
 * 
 * Version control:
 * 
 * 	Rev1:
 * 		- change the 2-D array to 1-D array
 * 
 */

#include <iostream>
using namespace std;

#include <iomanip>
#include <limits>
#include <string.h>
#include <sys/time.h>

#include <mpi.h>
#include <omp.h>

#include <fstream>
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
        
#define DATE_FORMAT "%Y%m%d_%H%M%S"
#define DATE_SIZE 20



// Main procedure
//----------------
int main(int argc, char** argv) {
	
	//times variables
	double time_ROI_begin, time_ROI_end;
	double time_begin, time_end;	
	struct timeval t;
	
	gettimeofday(&t,NULL);
	time_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	//variables for the arguments
	bool int_float;
	int minimum_value, maximum_value;
	int rows1, columns1;
	int output;
	int debug;
	int nodes;
	
	//number of rows of the result matrix
	int rows_result;
	
	
	if( argc != 9 ){
		cout << "Usage: " << endl 
			 << "mult_matrix <int or float> <minimum value> <maximum value> " 
			 << "<number of rows of matrix1> <number of columns of matrix1> "
			 << "<create output file> <debug> <number of nodes>" 
			 << endl;
			 
		return -1;
	}
	
	//TODO: check consistency of those values
	int_float = atoi(argv[1]);
	minimum_value = atoi(argv[2]);	
	maximum_value = atoi(argv[3]);
	rows1 = atoi(argv[4]);
	columns1 = atoi(argv[5]);	
	output = atoi(argv[6]);
	debug = atoi(argv[7]);
	nodes = atoi(argv[8]);	
	
	//the number of rows of the result is the same as the matrix A
	rows_result = rows1;
	
	if( debug >= 2 ){
		cout << "INPUTS: " << endl
			 << "Int or float: " << int_float << endl
			 << "Minimum value: " << minimum_value << endl
			 << "Maximum value: " << maximum_value << endl
			 << "Matrix 1: (" << rows1 << "x" << columns1 << ")" << endl
			 << "Output: " << output << endl
			 << "Debug: " << debug << endl 
			 << "Number of nodes: " << nodes << endl << endl;
	}
	
	//MPI variables
	int rank, comm_sz, len;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	
	// MPI initialization
	MPI_Init(&argc, &argv);
	
	//read the numbers of process
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	//read the rank of each process
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	//get the processor name
	MPI_Get_processor_name(hostname, &len);
	
	if( debug >= 2 ){
		cout << "Hello from process " << rank 
			 << " of " << comm_sz 
			 << " running in processor " << hostname << endl;
	}
	
	//Calculating the number of rows for each component ('A', 'x' and 'b')
	//TODO: check when this division isn't integer
	int delta_rows_matrix = rows1 / comm_sz;
	int delta_rows_vector = columns1 / comm_sz;
	int delta_rows_result = delta_rows_matrix;
	
	double *local_matrix;
	
	double *local_vector;
	double *global_vector;
	
	double *local_result;
	double *global_result;
  	
  	//Allocating the memory for submatrixes 'A'
  	local_matrix = new double [delta_rows_matrix*columns1];
  	
	
	//Allocating the memory for local vector and also for the global vector.
	//Note that all processes have the entire vector 'x', in order to calculate a valid output 'b'
	local_vector = new double [delta_rows_vector];
	global_vector = new double [columns1];
	
	//Allocating the memory for local result (vector 'b').
	//Note that only the rank 0 will have the entire result
	local_result = new double [delta_rows_result];
	if( rank == 0 ){
		global_result = new double [rows1];
	}
	
	
	if( debug >= 2 ){
		cout << "After dynamic allocation in rank " << rank << "." << endl << endl;
	}
	
	
	//GENERATING MATRIX A AND VECTOR X	
	srand((unsigned)time(0) + rank);
	
	for(int i = 0; i<delta_rows_matrix; i++){
		for(int j = 0; j<columns1;j++){
			
			if( int_float ){
				local_matrix[i*columns1 + j] = minimum_value + 
					( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
			}
			else{
				local_matrix[i*columns1 + j] = minimum_value + (rand()%maximum_value);
			}
		}
	}
	
	for(int i = 0; i<delta_rows_vector; i++){
		
		if( int_float ){
			local_vector[i] = minimum_value + 
				( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
		}
		else{
			local_vector[i] = minimum_value + (rand()%maximum_value);
		}
		
	}
	
	if( debug >= 3 ){
		cout << "GENERATED MATRIX'S" << endl << endl;
		
		cout << "Matrix A - Rank " << rank << ": " << endl;			
		for(int i = 0; i<delta_rows_matrix; i++){
			cout << "[";
			for(int j = 0; j<columns1;j++){
				cout << local_matrix[i*columns1 + j] << " ";
			}
			cout << "]" << endl;
		}
		
		cout << endl << "Vector x - Rank " << rank << ": " << endl;
		cout << "[";		
		for(int i = 0; i<delta_rows_vector; i++){
			cout << local_vector[i];
			if( i < delta_rows_vector -1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
	
	//Begin of the Region of Interest (ROI)
	
	gettimeofday(&t,NULL);
	time_ROI_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	MPI_Allgather( local_vector, delta_rows_vector, MPI_DOUBLE, 
				   global_vector, delta_rows_vector, MPI_DOUBLE, MPI_COMM_WORLD );
				   
	if( debug >= 3 ){
		cout << "Global vector - Rank " << rank <<": " << endl;
		cout << "[";		
		for(int i = 0; i<columns1; i++){
			cout << global_vector[i];
			if( i < columns1 -1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
		
	
	int nthreads;
	#pragma omp parallel
	{
		
		if( omp_get_thread_num() == 0 ){ 
			nthreads = omp_get_num_threads();
			
			if( debug >= 2 ){
				cout << endl << "Number of threads is: " << nthreads << endl;
			}
		}
		
		#pragma omp for reduction(+:local_result[:delta_rows_matrix]) collapse(2)
		for(int i = 0; i < delta_rows_matrix; i++){
			for(int k = 0; k < columns1; k++){
				local_result[i] += local_matrix[i*columns1 + k]*global_vector[k];
			}
		}
	
	}
	
	if( debug >= 3 ){
		cout << "Local result - Rank " << rank << ": " << endl;
		cout << "[";		
		for(int i = 0; i<delta_rows_matrix; i++){
			cout << local_result[i];
			if( i < delta_rows_matrix -1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
	
	MPI_Gather( local_result, delta_rows_result, MPI_DOUBLE, 
				global_result, delta_rows_result, MPI_DOUBLE, 0, MPI_COMM_WORLD );
	
	gettimeofday(&t,NULL);
	time_ROI_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	//End of the Region of Interest (ROI)
	  	
  	if( (debug >= 1) and (rank == 0) ){
		cout << "ROI time: " << time_ROI_end - time_ROI_begin << " seconds." << endl;
	}
  	
  	
  	if( (debug >= 3) and (rank == 0) ){		
		cout << endl << "RESULT" << endl;
		cout << "[";	
		for(int i = 0; i<rows_result; i++){
			cout << global_result[i];
			if( i < rows_result-1 ){
				cout << endl;
			}
		}
		cout << "]" << endl;
	}
	
	char date[DATE_SIZE];
	time_t now = time(0);
	strftime(date, sizeof(date), DATE_FORMAT, localtime(&now));
	char separator = ';';
	
	/* 
	 * For a debug purpose, the program has the possibility to save all the matrixes used in the computation, 
	 * including the inputs ('A' and 'x'). In this case, we need to gather the matrix 'A' in the rank 0, 
	 * to make it possible that this rank save the matrix in an output file. 
	 */
	if( output >= 2 ){
		
		double *global_matrix;
		global_matrix = new double [rows1*columns1];
		
		MPI_Gather( local_matrix, delta_rows_matrix*columns1, MPI_DOUBLE, 
					global_matrix, delta_rows_matrix*columns1, MPI_DOUBLE, 0, MPI_COMM_WORLD );
		
		if( rank == 0 ){
			//SAVING THE RESULT MATRIX
			string csv_name = string("../outputs/Rev1/") + string("result");
			csv_name += "_nodes_" + SSTR(nodes);
			csv_name += "_processes_" + SSTR(comm_sz);
			csv_name += "_threads_" + SSTR(nthreads);
			csv_name += "_size_" + SSTR(rows1);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << endl << csv_name << endl;
			}
			
			ofstream csv;
			csv.open(csv_name.c_str());
			
			for(int i=0; i<rows_result; i++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< global_result[i] << separator << endl;
			}
			
			csv.close();

			
			//SAVING THE MATRIX A
			csv_name = string("../inputs/Rev1/") + string("matrix_A");
			csv_name += "_nodes_" + SSTR(nodes);
			csv_name += "_processes_" + SSTR(comm_sz);
			csv_name += "_threads_" + SSTR(nthreads);
			csv_name += "_size_" + SSTR(rows1);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << csv_name << endl;
			}
			
			csv.open(csv_name.c_str());
			
			for(int i=0; i<rows1; i++){
				for(int j=0; j<columns1; j++){
					csv << setprecision(numeric_limits<double>::digits10) 
						<< local_matrix[i*columns1 + j] << separator;
				}
				csv << endl;
			}
			
			csv.close();
			
			
			//SAVING THE VECTOR X
			csv_name = string("../inputs/Rev1/") + string("vector_x");
			csv_name += "_nodes_" + SSTR(nodes);
			csv_name += "_processes_" + SSTR(comm_sz);
			csv_name += "_threads_" + SSTR(nthreads);
			csv_name += "_size_" + SSTR(rows1);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << csv_name << endl;
			}
			
			csv.open(csv_name.c_str());
			
			for(int i=0; i<columns1; i++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< global_vector[i] << separator << endl;
			}
			
			csv.close();
			
			
		
		}
		
		delete [] global_matrix;
	}
  	
  	
  	gettimeofday(&t,NULL);
	time_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	if( (debug >= 1) and (rank == 0) ){
		cout << endl << "Total execution time: " << time_end - time_begin << " seconds." << endl << endl;
	}
	
	
	if( (output >= 1) and (rank == 0) ){
		
		string csv_name = string("../outputs/Rev1/") + string("output");
		csv_name += "_nodes_" + SSTR(nodes);
		csv_name += "_processes_" + SSTR(comm_sz);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		csv << "ROI time" << separator << time_ROI_end - time_ROI_begin << endl;
		csv << "Total time" << separator << time_end - time_begin << endl;
		csv << "nodes" << separator << nodes << endl;
		csv << "processes" << separator << comm_sz << endl;
		csv << "threads" << separator << nthreads << endl;
		csv << "rows matrix 1" << separator << rows1 << endl;
		csv << "columns matrix 1" << separator << columns1 << endl;
		
		csv.close();
		
	}
	
	
	//Free the memory
	delete [] local_matrix;	
	
	delete [] local_vector;
	delete [] global_vector;
	
	delete [] local_result;
	if( rank == 0 ){
		delete [] global_result;
	}
	
	MPI_Finalize();
	
	
	return 0;
}
