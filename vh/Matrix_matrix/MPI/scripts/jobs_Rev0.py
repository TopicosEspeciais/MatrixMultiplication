import subprocess

for i in range(5):	
	nodes = 1
	for ntasks in [2, 4, 8, 16, 32]:
		for rows in [1024, 2048, 4096, 8192, 16384]:
			name = "job_MPI_test_Rev0_nodes_" + str(nodes) + "_ntasks_" + str(ntasks) + "_rows_" + str(rows) + ".sh"
			rc = subprocess.run(["sbatch", name])
			
	
	ntasks = 32
	for nodes in [2, 4]:
		for rows in [1024, 2048, 4096, 8192, 16384]:
			name = "job_MPI_test_Rev0_nodes_" + str(nodes) + "_ntasks_" + str(ntasks) + "_rows_" + str(rows) + ".sh"
			rc = subprocess.run(["sbatch", name])
