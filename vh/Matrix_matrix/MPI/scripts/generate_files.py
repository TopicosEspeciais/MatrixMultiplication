
nodes = 1
for ntasks in [2, 4, 8, 16, 32]:
	for rows in [1024, 2048, 4096, 8192, 16384]:
		
		file = open("job_MPI_test_Rev0_nodes_" + str(nodes) + "_ntasks_" + str(ntasks) + "_rows_" + str(rows) + ".sh", "w")
		
		file.write("#!/bin/bash\n")
		file.write("#SBATCH --job-name=MxM_MPI_R0\n")
		file.write("#SBATCH --output=../slurm_outputs/slurm-Rev0-" + 
				   "nodes-" + str(nodes) + "_ntasks_" + str(ntasks) + "-rows-" + str(rows) + "-%j.out\n")
		file.write("#SBATCH --nodes=" + str(nodes) + "\n")
		file.write("#SBATCH --ntasks-per-node=" + str(ntasks) + "\n")
		file.write("#SBATCH --hint=compute_bound\n")
		file.write("#SBATCH --mem=120G\n")
		file.write("#SBATCH --exclusive\n")
		file.write("#SBATCH --partition=test\n")
		file.write("#SBATCH --time=0-0:29\n")
		
		file.write("\nexport TMPDIR=/tmp/scratch/$USER\n")
		
		file.write("\nsrun ../matrix_matrix_MPI_Rev0 " +
				   "1 -10000 10000 " + str(rows) + " 1024 1024 1024 1 0 " + str(nodes))
		
		file.close()


ntasks = 32
for nodes in [2, 4]:
	for rows in [1024, 2048, 4096, 8192, 16384]:
		
		file = open("job_MPI_test_Rev0_nodes_" + str(nodes) + "_ntasks_" + str(ntasks) + "_rows_" + str(rows) + ".sh", "w")
		
		file.write("#!/bin/bash\n")
		file.write("#SBATCH --job-name=MxM_MPI_R0\n")
		file.write("#SBATCH --output=../slurm_outputs/slurm-Rev0-" + 
				   "nodes-" + str(nodes) + "_ntasks_" + str(ntasks) + "-rows-" + str(rows) + "-%j.out\n")
		file.write("#SBATCH --nodes=" + str(nodes) + "\n")
		file.write("#SBATCH --ntasks-per-node=" + str(ntasks) + "\n")
		file.write("#SBATCH --hint=compute_bound\n")
		file.write("#SBATCH --mem=120G\n")
		file.write("#SBATCH --exclusive\n")
		file.write("#SBATCH --partition=test\n")
		file.write("#SBATCH --time=0-0:29\n")
		
		file.write("\nexport TMPDIR=/tmp/scratch/$USER\n")
		
		file.write("\nsrun ../matrix_matrix_MPI_Rev0 " +
				   "1 -10000 10000 " + str(rows) + " 1024 1024 1024 1 0 " + str(nodes))
		
		file.close()

