#!/bin/bash
#SBATCH --job-name=MxM_MPI_R0
#SBATCH --output=../slurm_outputs/slurm-Rev0-nodes-1_ntasks_8-rows-16384-%j.out
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export TMPDIR=/tmp/scratch/$USER

srun ../matrix_matrix_MPI_Rev0 1 -10000 10000 16384 1024 1024 1024 1 0 1