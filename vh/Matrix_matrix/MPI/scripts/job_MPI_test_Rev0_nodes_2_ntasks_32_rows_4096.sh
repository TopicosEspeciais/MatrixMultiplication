#!/bin/bash
#SBATCH --job-name=MxM_MPI_R0
#SBATCH --output=../slurm_outputs/slurm-Rev0-nodes-2_ntasks_32-rows-4096-%j.out
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=32
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export TMPDIR=/tmp/scratch/$USER

srun ../matrix_matrix_MPI_Rev0 1 -10000 10000 4096 1024 1024 1024 1 0 2