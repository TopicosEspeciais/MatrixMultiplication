#!/bin/bash
#SBATCH --job-name=MxM_OMP_R0
#SBATCH --output=../slurm_outputs/slurm-Rev0-threads-8-rows-8192-%j.out
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export OMP_NUM_THREADS=8

srun ../matrix_matrix_OpenMP_Rev0 1 -10000 10000 8192 1024 1024 1024 1 0