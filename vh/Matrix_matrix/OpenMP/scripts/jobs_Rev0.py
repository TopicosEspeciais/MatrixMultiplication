import subprocess

for i in range(5):	
	for threads in [2, 4, 8, 16, 32]:
		for rows in [1024, 2048, 4096, 8192, 16384]:
			name = "job_OMP_test_Rev0_threads_" + str(threads) + "_rows_" + str(rows) + ".sh"
			rc = subprocess.run(["sbatch", name])
