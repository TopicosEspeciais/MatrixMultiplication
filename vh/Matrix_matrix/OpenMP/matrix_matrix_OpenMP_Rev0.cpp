/* 
 * Load modules:
 * module load compilers/gnu/6.4
 * 
 * Compile with:
 * g++ -o matrix_matrix_OpenMP_Rev0 matrix_matrix_OpenMP_Rev0.cpp -Wall -fopenmp
 * 
 * Usage:
 * matrix_matrix_OpenMP_Rev0 <int or float> <minimum value> <maximum value> 
 * 				 			 <nº of rows of matrix 1> <nº of columns of matrix 1> 
 * 				 			 <nº of rows of matrix 2> <nº of columns of matrix 2> 
 * 			     			 <create output file> <debug>
 * 
 * example: ./matrix_matrix_OpenMP_Rev0 0 0 5 3 2 2 3 0 3
 * 
 * 			Multiplies one matrix of integers, varying from 0 to 5, of size 3x2 to one matrix of integers of size 2x3, without creating an output file and with debug option 3 (prints time results (1), debug informations (2) and generated and resulted matrixes (3))
 * 
 * example: ./matrix_matrix_OpenMP_Rev0 1 -10000 10000 1024 1024 1024 1024 2 0 
 * 
 * 			Multiplies one matrix of floats, varying from -10000 to 10000 of size 1024x1024 to one vector of integers of size 1024x1, creating output files for generated and result matrixes and debug option 0 (don't print anyting)
 * 
 * Version control:
 * 
 * 
 */

#include <iostream>
using namespace std;

#include <iomanip>
#include <limits>
#include <string.h>
#include <sys/time.h>

#include <omp.h>

#include <fstream>
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
        
#define DATE_FORMAT "%Y%m%d_%H%M%S"
#define DATE_SIZE 20



// Main procedure
//----------------
int main(int argc, char** argv) {
	
	double time_ROI_begin, time_ROI_end;
	double time_begin, time_end;
	
	struct timeval t;
	
	gettimeofday(&t,NULL);
	time_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	bool int_float;
	int minimum_value, maximum_value;
	int rows1, columns1;
	int rows2, columns2;
	int output;
	int debug;
	
	int rows_result, columns_result;
	
	
	if( argc != 10 ){
		cout << "Usage: " << endl 
			 << "mult_matrix <int or float> <minimum value> <maximum value> " 
			 << "<number of rows of matrix 1> <number of columns of matrix 1> "
			 << "<number of rows of matrix 2> <number of columns of matrix 2> "
			 << "<create output file> <debug> " 
			 << endl;
			 
		return -1;
	}
	
	//TODO: check consistency of those values
	int_float = atoi(argv[1]);
	minimum_value = atoi(argv[2]);	
	maximum_value = atoi(argv[3]);
	rows1 = atoi(argv[4]);
	columns1 = atoi(argv[5]);	
	rows2 = atoi(argv[6]);
	columns2 = atoi(argv[7]);
	output = atoi(argv[8]);
	debug = atoi(argv[9]);
	
	if( columns1 != rows2 ){
		cout << "The number of columns of matrix1 must be equal to the number of rows of matrix2." << endl;
		return -1;
	}
	
	rows_result = rows1;
	columns_result = columns2;
	
	if( debug >= 2 ){
		cout << "INPUTS: " << endl
			 << "Int or float: " << int_float << endl
			 << "Minimum value: " << minimum_value << endl
			 << "Maximum value: " << maximum_value << endl
			 << "Matrix 1: (" << rows1 << "x" << columns1 << ")" << endl
			 << "Matrix 2: (" << rows2 << "x" << columns2 << ")" << endl
			 << "Output: " << output << endl
			 << "Debug: " << debug << endl << endl;
	}
	
	
  	
  	double *matrix1;
	double *matrix2;
	double *result;
  	
  	matrix1 = new double [rows1*columns1];	
	matrix2 = new double [rows2*columns2];
	
	result = new double [rows_result*columns_result];
	
	
	if( debug >= 2 ){
		cout << "After dynamic allocation." << endl;
	}
	
	
	//GENERATING MATRIX 1 AND MATRIX 2
	srand((unsigned)time(0));
	
	for(int i = 0; i<rows1; i++){
		for(int j = 0; j<columns1;j++){
			
			if( int_float ){
				matrix1[i*columns1 + j] = minimum_value + 
					( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
			}
			else{
				matrix1[i*columns1 + j] = minimum_value + (rand()%maximum_value);
			}
		}
	}
	
	for(int i = 0; i<rows2; i++){
		for(int j = 0; j<columns2;j++){
			
			if( int_float ){
				matrix2[i*columns2 + j] = minimum_value + 
					( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
			}
			else{
				matrix2[i*columns2 + j] = minimum_value + (rand()%maximum_value);
			}
		}
	}
	
	if( debug >= 3 ){
		cout << "GENERATED MATRIX'S" << endl << endl;
		
		cout << "Matrix 1: " << endl;			
		for(int i = 0; i<rows1; i++){
			cout << "[";
			for(int j = 0; j<columns1;j++){
				cout << matrix1[i*columns1 + j] << " ";
			}
			cout << "]" << endl;
		}
		
		cout << endl << "Matrix 1: " << endl;			
		for(int i = 0; i<rows2; i++){
			cout << "[";
			for(int j = 0; j<columns2;j++){
				cout << matrix2[i*columns2 + j] << " ";
			}
			cout << "]" << endl;
		}
	}
	
	//Begin of the Region of Interest (ROI)
	
	gettimeofday(&t,NULL);
	time_ROI_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	int nthreads;
	#pragma omp parallel
	{
		
		if( omp_get_thread_num() == 0 ){
			nthreads = omp_get_num_threads();			
			if( debug >= 2 ){			 
				cout << endl << "Number of threads is: " << nthreads << endl << endl;
			}
		}
		
		//the iterator of the outer 'for' needs to be unsigned long, because the collapse() directive makes one big 'for'
		#pragma omp for reduction(+:result[:rows1*columns2]) collapse(3)
		for(unsigned long int i = 0; i < (unsigned)rows1; i++){
			for(int j = 0; j < columns2; j++){
				for(int k = 0; k < columns1; k++){				
					result[i*columns2 + j] += matrix1[i*columns1 + k]*matrix2[k*columns2 + j];
				}
			}
		}
	}
	
	gettimeofday(&t,NULL);
	time_ROI_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	//End of the Region of Interest (ROI)
	  	
  	if( debug >= 1 ){
		cout << "ROI time: " << time_ROI_end - time_ROI_begin << " seconds." << endl;
	}
  	
  	
  	if( debug >= 3 ){
		
		cout << endl << "RESULT" << endl;		
		for(int i = 0; i<rows_result; i++){
			cout << "[";
			for(int j = 0; j<columns_result;j++){
				cout << result[i*columns_result + j] << " ";
			}
			cout << "]" << endl;
		}
	}
	
	char date[DATE_SIZE];
	time_t now = time(0);
	strftime(date, sizeof(date), DATE_FORMAT, localtime(&now));
	char separator = ';';
	
	if( output >= 2 ){
		
		//SAVING THE RESULT MATRIX
		string csv_name = string("../outputs/Rev0/") + string("result");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << endl << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		for(int i=0; i<rows_result; i++){
			for(int j=0; j<columns_result; j++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< result[i*columns_result + j] << separator;
			}
			csv << endl;
		}
		
		csv.close();
		
		
		//SAVING THE MATRIX 1
		csv_name = string("../inputs/Rev0/") + string("matrix_1");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		csv.open(csv_name.c_str());
		
		for(int i=0; i<rows1; i++){
			for(int j=0; j<columns1; j++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< matrix1[i*columns1 + j] << separator;
			}
			csv << endl;
		}
		
		csv.close();
		
		
		//SAVING THE MATRIX 2
		csv_name = string("../inputs/Rev0/") + string("matrix_2");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows2);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		csv.open(csv_name.c_str());
		
		for(int i=0; i<rows2; i++){
			for(int j=0; j<columns2; j++){
				csv << setprecision(numeric_limits<double>::digits10) 
					<< matrix2[i*columns2 + j] << separator;
			}
			csv << endl;
		}
		
		csv.close();
		
	}
  	
  	
  	gettimeofday(&t,NULL);
	time_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	if( debug >= 1 ){
		cout << "Total execution time: " << time_end - time_begin << " seconds." << endl;
	}
	
	
	if( output >= 1 ){
		
		string csv_name = string("../outputs/Rev0/") + string("output");
		csv_name += "_nodes_" + SSTR(1);
		csv_name += "_processes_" + SSTR(1);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		csv << "ROI time" << separator << time_ROI_end - time_ROI_begin << endl;
		csv << "Total time" << separator << time_end - time_begin << endl;
		csv << "nodes" << separator << 1 << endl;
		csv << "processes" << separator << 1 << endl;
		csv << "threads" << separator << nthreads << endl;
		csv << "rows matrix 1" << separator << rows1 << endl;
		csv << "columns matrix 1" << separator << columns1 << endl;
		csv << "rows matrix 2" << separator << rows2 << endl;
		csv << "columns matrix 2" << separator << columns2 << endl;
		
		csv.close();
		
	}
	
	//Free the memory
	delete [] matrix1;		
	delete [] matrix2;
	
	delete [] result;
	
	
	return 0;
}
