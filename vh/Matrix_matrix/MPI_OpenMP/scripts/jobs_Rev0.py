import subprocess

for i in range(5):	
	for nodes in [2, 4]:
		for threads in [2, 4, 8, 16, 32]:
			for rows in [1024, 2048, 4096, 8192, 16384]:
				name = ("job_MPI_OpenMP_test_Rev0" + 
						"_nodes_" + str(nodes) + "_threads_" + str(threads) + "_rows_" + str(rows) + ".sh")
				rc = subprocess.run(["sbatch", name])
