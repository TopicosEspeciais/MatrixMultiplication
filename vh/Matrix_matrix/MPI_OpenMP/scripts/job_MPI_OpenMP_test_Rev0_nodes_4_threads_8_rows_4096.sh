#!/bin/bash
#SBATCH --job-name=MxM_OMP_MPI_R0
#SBATCH --output=../slurm_outputs/slurm-Rev0-_nodes_4_threads_8_rows_4096-%j.out
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

export OMP_NUM_THREADS=8

srun ../matrix_matrix_MPI_OpenMP_Rev0 1 -10000 10000 4096 1024 1024 1024 1 0 4