/* 
 * Load modules:
 * module load compilers/gnu/6.4
 * module load libraries/openmpi/2.1.1-gnu-6.4
 * 
 * Compile with:
 * mpic++ -o matrix_matrix_MPI_OpenMP_Rev0 matrix_matrix_MPI_OpenMP_Rev0.cpp -Wall -fopenmp
 * 
 * Usage:
 * matrix_matrix_MPI_Rev0 <int or float> <minimum value> <maximum value> 
 * 				 		  <nº of rows of matrix 1> <nº of columns of matrix 1> 
 * 				 		  <nº of rows of matrix 2> <nº of columns of matrix 2> 
 * 			     		  <create output file> <debug> <nodes>
 * 
 * example: ./matrix_matrix_MPI_Rev0 0 0 5 3 2 2 3 0 3 2
 * 
 * 			Multiplies one matrix of integers, varying from 0 to 5, of size 3x2 to one matrix of integers of size 2x3, without creating an output file, with debug option 3 (prints time results (1), debug informations (2) and generated and resulted matrixes (3)) and in 2 different nodes
 * 
 * example: ./matrix_matrix_Rev1 1 -10000 10000 1024 1024 1024 1024 2 0 2
 * 
 * 			Multiplies one matrix of floats, varying from -10000 to 10000 of size 1024x1024 to one vector of integers of size 1024x1, creating output files for generated and result matrixes, with debug option 0 (don't print anyting) and in 2 different nodes
 * 
 * Version control:
 * 
 * 
 */

#include <iostream>
using namespace std;

#include <iomanip>
#include <limits>
#include <string.h>
#include <sys/time.h>
#include <stdlib.h>

#include <mpi.h>
#include <omp.h>

#include <fstream>
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
        
#define DATE_FORMAT "%Y%m%d_%H%M%S"
#define DATE_SIZE 20



// Main procedure
//----------------
int main(int argc, char** argv) {
	
	double time_ROI_begin, time_ROI_end;
	double time_begin, time_end;
	
	struct timeval t;
	
	gettimeofday(&t,NULL);
	time_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	bool int_float;
	int minimum_value, maximum_value;
	int rows1, columns1;
	int rows2, columns2;
	int output;
	int debug;
	int nodes;
	
	int rows_result, columns_result;
	
	
	if( argc != 11 ){
		cout << "Usage: " << endl 
			 << "mult_matrix <int or float> <minimum value> <maximum value> " 
			 << "<number of rows of matrix 1> <number of columns of matrix 1> "
			 << "<number of rows of matrix 2> <number of columns of matrix 2> "
			 << "<create output file> <debug> <number of nodes> " 
			 << endl;
			 
		return -1;
	}
	
	//TODO: check consistency of those values
	int_float = atoi(argv[1]);
	minimum_value = atoi(argv[2]);	
	maximum_value = atoi(argv[3]);
	rows1 = atoi(argv[4]);
	columns1 = atoi(argv[5]);	
	rows2 = atoi(argv[6]);
	columns2 = atoi(argv[7]);
	output = atoi(argv[8]);
	debug = atoi(argv[9]);
	nodes = atoi(argv[10]);
	
	if( columns1 != rows2 ){
		cout << "The number of columns of matrix1 must be equal to the number of rows of matrix2." << endl;
		return -1;
	}
	
	rows_result = rows1;
	columns_result = columns2;
	
	if( debug >= 2 ){
		cout << "INPUTS: " << endl
			 << "Int or float: " << int_float << endl
			 << "Minimum value: " << minimum_value << endl
			 << "Maximum value: " << maximum_value << endl
			 << "Matrix 1: (" << rows1 << "x" << columns1 << ")" << endl
			 << "Matrix 2: (" << rows2 << "x" << columns2 << ")" << endl
			 << "Output: " << output << endl
			 << "Debug: " << debug << endl 
			 << "Nodes: " << nodes << endl << endl;
	}
	
	
	//MPI variables
	int rank, comm_sz, len;
	char hostname[MPI_MAX_PROCESSOR_NAME];
	
	//OpenMP variables
	int nthreads;
	nthreads = atoi( getenv( "OMP_NUM_THREADS" ) );
	
	// MPI initialization
	MPI_Init(&argc, &argv);
	
	//read the numbers of process
	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
	//read the rank of each process
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	//get the processor name
	MPI_Get_processor_name(hostname, &len);
	
	
	if( debug >= 2 ){
		cout << "Hello from process " << rank 
			 << " of " << comm_sz 
			 << " running in processor " << hostname << endl;
	}
	
	//Calculating the number of rows for each component ('A', 'B' and 'C')
	//TODO: check when this division isn't integer
	int local_rows1 = rows1 / comm_sz;
	int local_rows2 = rows2 / comm_sz;
	int local_rows_result = local_rows1;
	
  	
  	double *matrix1;
	double *matrix2;
	double *local_result;
	double *global_result;
  	
  	matrix1 = new double [local_rows1*columns1];	
	matrix2 = new double [local_rows2*columns2];	
	local_result = new double [local_rows_result*columns_result];
	for(int i = 0; i < local_rows_result*columns_result; i++){
		local_result[i] = 0;
	}
	
	//only allocate the memory of the global result in the rank 0
	if( rank == 0 ){
		global_result = new double [rows_result*columns_result];
	}
	
	
	if( debug >= 2 ){
		cout << "After dynamic allocation." << endl << endl;
	}
	
	
	//GENERATING MATRIX 1 AND MATRIX 2
	srand( ((unsigned)time(0) + rank)*(rank + 1) );
	
	for(int i = 0; i<local_rows1; i++){
		for(int j = 0; j<columns1;j++){
			
			if( int_float ){
				matrix1[i*columns1 + j] = minimum_value + 
					( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
			}
			else{
				matrix1[i*columns1 + j] = minimum_value + (rand()%maximum_value);
			}
		}
	}
	
	for(int i = 0; i<local_rows2; i++){
		for(int j = 0; j<columns2;j++){
			
			if( int_float ){
				matrix2[i*columns2 + j] = minimum_value + 
					( (double)rand() / ( (double)( RAND_MAX /(maximum_value-minimum_value) )) );
			}
			else{
				matrix2[i*columns2 + j] = minimum_value + (rand()%maximum_value);
			}
		}
	}
	
	if( debug >= 3 ){
		cout << "GENERATED MATRIX'S - Rank " << rank << ": " << endl << endl;
		
		cout << "Matrix 1: " << endl;			
		for(int i = 0; i<local_rows1; i++){
			cout << "[";
			for(int j = 0; j<columns1;j++){
				cout << matrix1[i*columns1 + j] << " ";
			}
			cout << "]" << endl;
		}
		
		cout << endl << "Matrix 2: " << endl;			
		for(int i = 0; i<local_rows2; i++){
			cout << "[";
			for(int j = 0; j<columns2;j++){
				cout << matrix2[i*columns2 + j] << " ";
			}
			cout << "]" << endl;
		}
	}

	
	if( debug >= 2 ){
		cout << endl << "Number of threads is: " << nthreads << endl;
		cout << "Beginning of the ROI." << endl << endl;
	}
	
	//Beginning of the Region of Interest (ROI)
	gettimeofday(&t,NULL);
	time_ROI_begin = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	//Define a derived datatype which correspond to one column of the matrix2
	MPI_Datatype MPI_column2;
	MPI_Type_vector( local_rows2, 1, columns2, MPI_DOUBLE, &MPI_column2 );
	MPI_Type_commit( &MPI_column2 );
	
	
		
	//temporary vector that will hold each column of the (global) matrix2
	double *tmp_column;
	tmp_column = new double[rows2];
	
	//iterate over the columns of the (local) matrix2
	for(int j = 0; j < columns2; j++){
		
		//Gather each column of the (global) matrix2 in the temporary column, for all the processes
		MPI_Allgather( &matrix2[j], 1, MPI_column2, 
					   tmp_column, local_rows2, MPI_DOUBLE, MPI_COMM_WORLD );
		
		if( debug >= 3 ){
			cout << "Column " << j << " in rank " << rank << endl;
			
			for(int k = 0; k < rows2; k++){
				cout << "[" << tmp_column[k] << "]" << endl;
			}
			cout << endl;
		}
		
		
		#pragma omp parallel
		{		
			
			if( omp_get_thread_num() == 0 ){ 
				nthreads = omp_get_num_threads();
				
				if( debug >= 2 ){
					cout << endl << "Number of threads is: " << nthreads << endl;
				}
			}
			
			//calculates the final result of the partial matrix C
			#pragma omp for reduction(+:local_result[:local_rows_result*columns_result]) collapse(2)
			for(int i = 0; i < local_rows1; i++){
				for(int k = 0; k < columns1; k++){
					local_result[i*columns2 + j] += matrix1[i*columns1 + k]*tmp_column[k];			
				}			
			}
			
		}
	}
	
	delete [] tmp_column;
	
	if( debug >= 3 ) {
		
		cout << "Result matrix of rank " << rank << endl;
		for(int i = 0; i < local_rows_result; i++){
			cout << "[ ";
			for(int j = 0; j < columns_result; j++){
				cout << local_result[i*columns_result + j] << " ";
			}
			cout << "]" << endl;
		}
		
	}
	
	//Gather all the local results in one global result
	MPI_Gather( local_result, local_rows_result*columns_result, MPI_DOUBLE, 
				global_result, local_rows_result*columns_result, MPI_DOUBLE, 0, MPI_COMM_WORLD );
	
	
	gettimeofday(&t,NULL);
	time_ROI_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	//End of the Region of Interest (ROI)
	
	if( (debug >= 3) and (rank == 0) ){
		cout << "Global result matrix" << endl;
		for(int i = 0; i < rows_result; i++){
			cout << "[ ";
			for(int j = 0; j < columns_result; j++){
				cout << global_result[i*columns_result + j] << " ";
			}
			cout << "]" << endl;
		}
	}
	
  	if( debug >= 1 ){
		cout << "ROI time of rank " << rank << ": " 
			 << time_ROI_end - time_ROI_begin << " seconds." << endl;
	}
  	
  	
	char date[DATE_SIZE];
	time_t now = time(0);
	strftime(date, sizeof(date), DATE_FORMAT, localtime(&now));
	char separator = ';';
	
	
	/* 
	 * For a debug purpose, the program has the possibility to save all the matrixes used in the computation, 
	 * including the inputs ('A' and 'B'). In this case, we need to gather the matrixes in the rank 0, 
	 * to make it possible that this rank save the matrixes in output files. 
	 */
	if( output >= 2 ){
		
		double *global_matrix1;
		global_matrix1 = new double [rows1*columns1];
		
		MPI_Gather( matrix1, local_rows1*columns1, MPI_DOUBLE, 
					global_matrix1, local_rows1*columns1, MPI_DOUBLE, 0, MPI_COMM_WORLD );
		
		
		double *global_matrix2;
		global_matrix2 = new double [rows2*columns2];
		
		MPI_Gather( matrix2, local_rows2*columns2, MPI_DOUBLE, 
					global_matrix2, local_rows2*columns2, MPI_DOUBLE, 0, MPI_COMM_WORLD );
		
		
		if( rank == 0 ){
			
			//SAVING THE RESULT MATRIX
			string csv_name = string("../outputs/Rev0/") + string("result");
			csv_name += "_nodes_" + SSTR(nodes);
			csv_name += "_processes_" + SSTR(comm_sz);
			csv_name += "_threads_" + SSTR(nthreads);
			csv_name += "_size_" + SSTR(rows1);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << endl << csv_name << endl;
			}
			
			ofstream csv;
			csv.open(csv_name.c_str());
			
			for(int i=0; i<rows_result; i++){
				for(int j=0; j<columns_result; j++){
					csv << setprecision(numeric_limits<double>::digits10) 
						<< global_result[i*columns_result + j] << separator;
				}
				csv << endl;
			}
			
			csv.close();
			
			
			//SAVING THE MATRIX 1
			csv_name = string("../inputs/Rev0/") + string("matrix_1");
			csv_name += "_nodes_" + SSTR(nodes);
			csv_name += "_processes_" + SSTR(comm_sz);
			csv_name += "_threads_" + SSTR(nthreads);
			csv_name += "_size_" + SSTR(rows1);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << csv_name << endl;
			}
			
			csv.open(csv_name.c_str());
			
			for(int i=0; i<rows1; i++){
				for(int j=0; j<columns1; j++){
					csv << setprecision(numeric_limits<double>::digits10) 
						<< global_matrix1[i*columns1 + j] << separator;
				}
				csv << endl;
			}
			
			csv.close();
			
			
			//SAVING THE MATRIX 2
			csv_name = string("../inputs/Rev0/") + string("matrix_2");
			csv_name += "_nodes_" + SSTR(nodes);
			csv_name += "_processes_" + SSTR(comm_sz);
			csv_name += "_threads_" + SSTR(nthreads);
			csv_name += "_size_" + SSTR(rows2);
			csv_name += "_" + string(date);		
			csv_name += ".csv";
			
			if( debug >= 2 ){
				cout << csv_name << endl;
			}
			
			csv.open(csv_name.c_str());
			
			for(int i=0; i<rows2; i++){
				for(int j=0; j<columns2; j++){
					csv << setprecision(numeric_limits<double>::digits10) 
						<< global_matrix2[i*columns2 + j] << separator;
				}
				csv << endl;
			}
			
			csv.close();
		}
		
		delete [] global_matrix1;
		delete [] global_matrix2;
		
	}
  	
  	
  	gettimeofday(&t,NULL);
	time_end = (double)t.tv_sec+(double)t.tv_usec*1e-6;
	
	if( debug >= 1 ){
		cout << "Total execution time: " << time_end - time_begin << " seconds." << endl;
	}
	
	
	if( (output >= 1) and (rank == 0) ){
		
		string csv_name = string("../outputs/Rev0/") + string("output");
		csv_name += "_nodes_" + SSTR(nodes);
		csv_name += "_processes_" + SSTR(comm_sz);
		csv_name += "_threads_" + SSTR(nthreads);
		csv_name += "_size_" + SSTR(rows1);
		csv_name += "_" + string(date);		
		csv_name += ".csv";
		
		if( debug >= 2 ){
			cout << csv_name << endl;
		}
		
		ofstream csv;
		csv.open(csv_name.c_str());
		
		csv << "ROI time" << separator << time_ROI_end - time_ROI_begin << endl;
		csv << "Total time" << separator << time_end - time_begin << endl;
		csv << "nodes" << separator << nodes << endl;
		csv << "processes" << separator << comm_sz << endl;
		csv << "threads" << separator << nthreads << endl;
		csv << "rows matrix 1" << separator << rows1 << endl;
		csv << "columns matrix 1" << separator << columns1 << endl;
		csv << "rows matrix 2" << separator << rows2 << endl;
		csv << "columns matrix 2" << separator << columns2 << endl;
		
		csv.close();
		
	}
	
	
	//Free the memory
	delete [] matrix1;		
	delete [] matrix2;
	
	delete [] local_result;
	if( rank == 0 ){
		delete [] global_result;
	}
	
	MPI_Finalize();
	
	return 0;
}
