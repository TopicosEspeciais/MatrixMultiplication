
for rows in [1024, 2048, 4096, 8192, 16384]:
	file = open("job_Serial_test_Rev1_rows_" + str(rows) + ".sh", "w")
	
	file.write("#!/bin/bash\n")
	file.write("#SBATCH --job-name=MxM_S_R1\n")
	file.write("#SBATCH --output=../slurm_outputs/slurm-Rev1-rows-" + str(rows) + "-%j.out\n")
	file.write("#SBATCH --nodes=1\n")
	file.write("#SBATCH --ntasks-per-node=1\n")
	file.write("#SBATCH --cpus-per-task=1\n")
	file.write("#SBATCH --hint=compute_bound\n")
	file.write("#SBATCH --mem=120G\n")
	file.write("#SBATCH --exclusive\n")
	file.write("#SBATCH --partition=test\n")
	file.write("#SBATCH --time=0-0:29\n")
	
	file.write("\nexport TMPDIR=/tmp/scratch/$USER\n")
	
	file.write("\nsrun ../matrix_matrix_Rev1 1 -10000 10000 " + str(rows) + " 1024 1024 1024 1 0")
	
	file.close()

