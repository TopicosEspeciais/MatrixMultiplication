#!/bin/bash
#SBATCH --job-name=MxM_S_R1
#SBATCH --output=../slurm_outputs/slurm-Rev1-rows-16384-%j.out
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --hint=compute_bound
#SBATCH --mem=120G
#SBATCH --exclusive
#SBATCH --partition=test
#SBATCH --time=0-0:29

srun ../matrix_matrix_Rev1 1 -10000 10000 16384 1024 1024 1024 1 0